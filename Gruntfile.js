module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        bowercopy: {
            options: {
                srcPrefix: 'bower_components',
                destPrefix: 'app/Resources/'
            },
            scripts: {
                files: {
                    'js/jquery.js': 'jquery/dist/jquery.js',
                    'js/bootstrap.js': 'bootstrap/dist/js/bootstrap.js'
                }
            },
            stylesheets: {
                files: {
                    'css/bootstrap.css': 'bootstrap/dist/css/bootstrap.css',
                    'css/font-awesome.css': 'font-awesome/css/font-awesome.css'
                }
            },
            fonts: {
                options: {
                    destPrefix: 'web/assets/'
                },
                files: [
                    {'fonts': 'font-awesome/fonts'},
                    {'fonts': 'bootstrap/fonts'},
                ],
            }
        },
        cssmin : {
            bundled:{
                src: 'web/assets//css/bundled.css',
                dest: 'web/assets/css/bundled.min.css'
            }
        },
        uglify : {
            js: {
                files: {
                    'web/assets/js/bundled.min.js': ['web/assets/js/bundled.js']
                }
            }
        },
        concat: {
            options: {
                stripBanners: true
            },
            css: {
                src: 'app/Resources/css/*',
                dest: 'web/assets/css/bundled.css'
            },
            js : {
                src : [
                    'app/Resources/js/jquery.js',
                    'app/Resources/js/bootstrap.js'
                ],
                dest: 'web/assets/js/bundled.js'
            }
        },
        copy: {
            files: {
                src: 'app/Resources/js/d3.min.js',
                dest: 'web/assets/js/d3.min.js'
            }
        },
        watch: {
            scripts: {
                files: ['app/Resources/js/*.js'],
                tasks: ['default'],
            },
            css: {
                files: 'app/Resources/css/*.css',
                tasks: ['default'],
            },
        },
    });

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['bowercopy','copy', 'concat', 'cssmin', 'uglify', 'watch']);
};
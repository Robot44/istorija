<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\Timeline;
use AppBundle\Form\EventType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/event")
 */
class EventController extends Controller
{
    /**
     * @Route("/add/{id}")
     * @param Request $request
     * @ParamConverter("timeline", class="AppBundle:Timeline")
     * @Template
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request, Timeline $timeline)
    {
        $em = $this->getDoctrine()->getManager();

        $event = new Event();
        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $timeline->addEvent($event);
            $em->persist($timeline);
            $em->persist($event);
            $em->flush();
        }

        $events = $em->getRepository('AppBundle:Event')
            ->findEventsBy(['timeline' => $timeline]);

        return ['form' => $form->createView(), 'events' => $events, 'timeline' => $timeline];
    }
}
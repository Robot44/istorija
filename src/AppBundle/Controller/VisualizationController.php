<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Timeline;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/visualization")
 */
class VisualizationController extends Controller
{
    /**
     * @Route("/simple")
     * @Template
     * @return array
     */
    public function simpleTimelinesAction() {
        $timelines = $this->getDoctrine()->getRepository('AppBundle:Timeline')->findAll();

        return array('timelines' => $timelines);
    }

    /**
     * @Route("/simple/{id}")
     * @Template
     * @return array
     */
    public function simpleTimelineAction(Timeline $timeline) {
        $events = $this->getDoctrine()->getRepository('AppBundle:Event')->findEventsBy(['timeline' => $timeline]);

        return array('timeline' => $timeline, 'events' => $events);
    }

    /**
     * @Route("/trait")
     * @Template
     * @return array
     */
    public function traitTimelineAction()
    {
        $timelines =  $this->getDoctrine()->getRepository('AppBundle:Timeline')->findAll();

        return ['timelines' => $timelines];
    }
}
<?php

namespace AppBundle\Controller;

trait DoctrineTrait
{
    function repo($repository)
    {
        return $this->getDoctrine()->getRepository($repository);
    }

    function remove($element)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($element);
        $em->flush();
    }

    function edit($element)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($element);
        $em->flush();
    }
}
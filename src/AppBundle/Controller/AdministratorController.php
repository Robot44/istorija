<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/")
 */
class AdministratorController extends Controller
{
    /**
     * @Route("/index")
     * @Template
     */
    public function indexAction(Request $request)
    {
        return ['message' => 'some text'];
    }
}

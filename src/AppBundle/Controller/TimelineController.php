<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Timeline;
use AppBundle\Form\TimelineType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/timeline")
 * @Template
 */
class TimelineController extends Controller
{
    /**
     * @Route("/new")
     * @Template
     */
    public function newAction(Request $request)
    {
        $timeline = new Timeline();
        $form = $this->createForm(TimelineType::class, $timeline);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($timeline);
            $em->flush();

            return $this->redirectToRoute('app_administrator_index');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/index")
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        $timelines = $this->getDoctrine()->getRepository('AppBundle:Timeline')->findAll();

        $timeline = new Timeline();
        $form = $this->createForm(TimelineType::class, $timeline);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($timeline);
            $em->flush();
        }

        return array('form' => $form->createView() ,'timelines' => $timelines);
    }

    /**
     * @Route("/{id}/edit")
     * @ParamConverter("timeline", class="AppBundle:Timeline")
     * @param Timeline $timeline
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Timeline $timeline, Request $request)
    {
        $form = $this->createForm(TimelineType::class, $timeline);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($timeline);
            $em->flush();

            return $this->redirectToRoute('app_timeline_index');
        }

        return array('form' => $form->createView());
    }
    /**
     * @Route("/{id}/remove")
     * @ParamConverter("timeline", class="AppBundle:Timeline")
     * @param Timeline $timeline
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Timeline $timeline, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($timeline);
        $em->flush();

        return $this->redirectToRoute('app_timeline_index');
    }


}
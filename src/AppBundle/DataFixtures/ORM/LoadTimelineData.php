<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Event;
use AppBundle\Entity\Timeline;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTimelineData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $timeline = new Timeline();
        $timeline->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
        $timeline->setTitle('Neque porro.');

        for ($i = 0; $i < 10; $i++) {
            $event = new Event();
            $event->setTitle($this->random_lipsum(1, 'words'));
            $event->setShortDescription($this->random_lipsum(5, 'words'));
            $event->setLongDescription($this->random_lipsum(20, 'words'));
            $int = rand(1262055681,1262055681);
            $event->setDate(new \DateTime(date("Y-m-d H:i:s",$int)));
            $manager->persist($event);
            $timeline->addEvent($event);
        }
        $manager->persist($timeline);
        $manager->flush();
    }

    function random_lipsum($amount = 1, $what = 'paras', $start = 0)
    {
        return simplexml_load_file("http://www.lipsum.com/feed/xml?amount=$amount&what=$what&start=$start")->lipsum;
    }
}
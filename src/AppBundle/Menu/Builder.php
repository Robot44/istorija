<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->addChild('Home', ['route' => 'app_administrator_index']);
        $menu->addChild('All timelines', ['route' => 'app_timeline_index']);

        $visualization = $menu->addChild('Visualization');
        $visualization->addChild('Simple', ['route' => 'app_visualization_simpletimelines']);
        $visualization->addChild('Trait', ['route' => 'app_visualization_traittimeline']);

        return $menu;
    }
}